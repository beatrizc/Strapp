<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Viaje extends Model
{
    //
    protected $hidden=['created_at', 'updated_at'];
    protected $fillable = ['numeroplazas', 'origen', 'destino', 'precio'];
  
}
