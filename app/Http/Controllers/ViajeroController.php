<?php

namespace App\Http\Controllers;

use App\Viajero;
use Illuminate\Http\Request;

class ViajeroController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
          $viajero = Viajero::all();
        
        if (!$viajero){
          return Response()->json(['mensaje'=>'No tiene un viajero asociado', 'codigo'=>404],404);
        }
        
        //retornamos el objeto viajero
        return Response()->json(['data'=>$viajero],200);

    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
          $this->validate($request, [
        'cedula' => 'required',
        'nombre' => 'required',
        'direccion' => 'required',
        'telefono' => 'required'
        
    ]);
        
         $viajero = Viaje::create($request->all());
 
        return response()->json($viajero, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Viajero  $viajero
     * @return \Illuminate\Http\Response
     */
    public function show(Viajero $viajero)
    {
        //
        $viajero = Viaje::find($viajero);
        
        if (!$viajero){
          return Response()->json(['mensaje'=>'No se encuentra el viajero', 'codigo'=>404],404);
        }
        //retornamos el objeto viajero
        return Response()->json(['data'=>$viajero],200);
    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Viajero  $viajero
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Viajero $viajero)
    {
        //
         $viajero->update($request->all());
 
        return response()->json($viajero, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Viajero  $viajero
     * @return \Illuminate\Http\Response
     */
    public function destroy(Viajero $viajero)
    {
        //
         $viajero->delete();
        return response()->json(null, 204);

    }
}
