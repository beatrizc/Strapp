<?php

namespace App\Http\Controllers;

use App\Viaje;
use Illuminate\Http\Request;

class ViajeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
        
        $viaje = Viaje::all();
        
        if (!$viaje){
          return Response()->json(['mensaje'=>'No tiene un viaje asociado', 'codigo'=>404],404);
        }
        
        //retornamos el objeto viajero
        return Response()->json(['data'=>$viaje],200);
    }

    
    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //  
         $this->validate($request, [
        'numeroplazas' => 'required',
        'origen' => 'required',
        'destino' => 'required',
        'price' => 'required'
        
    ]);
        
         $viaje = Viaje::create($request->all());
 
        return response()->json($viaje, 201);
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Viaje  $viaje
     * @return \Illuminate\Http\Response
     */
    public function show(Viaje $viaje)
    {
        //
        $viaje = Viaje::find($viaje);
        
        if (!$viaje){
          return Response()->json(['mensaje'=>'No tiene un viaje asociado', 'codigo'=>404],404);
        }
        //retornamos el objeto viaje
        return Response()->json(['data'=>$viaje],200);

    }

    

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Viaje  $viaje
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Viaje $viaje)
    {
        //
         $viaje->update($request->all());
 
        return response()->json($viaje, 200);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Viaje  $viaje
     * @return \Illuminate\Http\Response
     */
    public function destroy(Viaje $viaje)
    {
        //
          $viaje->delete();
 
        return response()->json(null, 204);
    }
}
