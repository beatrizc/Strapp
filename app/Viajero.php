<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Viajero extends Model
{
    //
    protected $hidden=['created_at', 'updated_at'];
    protected $fillable = ['cedula', 'nombre', 'direccion', 'telefono'];            
}
