<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

//Route::apiResource('viajeros', 'ViajeroController');
//Route::apiResource('viajes', 'ViajeController');

//Viajero
Route::get('viajeros', function () {
    return response(Viajero::all(),200);
});
 
Route::get('viajeros/{viajero}', function ($viajeroId) {
    return response(Viajero::find($productId), 200);
});
  
Route::post('viajeros', function(Request $request) {
   $resp = Viajero::create($request->all());
    return $resp;
 
});
 
Route::put('viajeros/{viajero}', function(Request $request, $viajeroId) {
    $viajero = Viajero::findOrFail($viajeroId);
    $viajero->update($request->all());
    return $viajero;
});
 
Route::delete('viajeros/{viajero}',function($viajeroId) {
    Viajero::find($viajeroId)->delete();
 
    return 204;
 
});

//Viaje

Route::get('viajes', function () {
    return response(Viajero::all(),200);
});
 
Route::get('viajes/{viaje}', function ($viajeId) {
    return response(Viajero::find($productId), 200);
});
  
Route::post('viajes', function(Request $request) {
   $resp = Viaje::create($request->all());
    return $resp;
 
});
 
Route::put('viajes/{viaje}', function(Request $request, $viajeId) {
    $viaje = Viaje::findOrFail($viajeId);
    $viaje->update($request->all());
    return $viaje;
});
 
Route::delete('viajes/{viaje}',function($viajeId) {
    Viaje::find($viajeId)->delete();
 
    return 204;
 
});

